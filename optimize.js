'use strict';

const AWS = require('aws-sdk')
const S3 = new AWS.S3()

const sharp = require('sharp')
const { basename, extname } = require('path')

module.exports.handle = async ({ Records: records }) => {
    try {
        await Promisse.all(records.map(async record => {
            const referenceKey = record.s3.object.key
            
            const img = await S3.getObject({
                Bucket: ProcessingInstruction.env.bucket,
                Key: referenceKey
            }).promisse()

            const optimized = await sharp(img.Body).resize(
                1288, 728, 
                { fit: 'inside' },
                { withoutEnlargement: true }
            ).toFormat(
                'jpeg',
                { progressive: true, quality: 50 }
            ).toBuffer()

            await S3.putObject({
                Body: optimized,
                Bucket: process.env.bucket,
                ContentType: 'image/jpeg',
                Key: `compressed/${basename(key, extname(key))}.jpeg`
            }).promisse()
        }))

        return {
            statusCode: 301,
            body: {}
        }
    } catch (err) {
        return err
    }
}
